let current=0;

function TouchThumbnails(list) {
  var startPos;
  list.addEventListener('touchstart', function(event){
    startPos = event.touches[0].clientX;
    pos = parseInt(list.style.marginLeft);
  });
  list.addEventListener('touchmove', function(event){
    newPos = pos+parseInt((event.touches[0].clientX)-startPos);
    if (newPos > 0) {
      list.style.marginLeft = '0px';
    } else if (newPos <= window.innerWidth-list.offsetWidth) {
      list.style.marginLeft = window.innerWidth-list.offsetWidth+'px';
    } else {0
      list.style.marginLeft = newPos+'px';
    }
  });
}

const ThubnailsPositioning = (index, custom) => {
	const thumbnailWidth = list.offsetHeight
	const thumbListWidth = list.offsetWidth
	const windowWidth = window.innerWidth
	const maxLeft = windowWidth - thumbListWidth;
	const scrollLeft = document.getElementsByClassName('scrollleft')[0]
	const scrollRight = document.getElementsByClassName('scrollright')[0]
	let goto;
	if (typeof(custom) != "undefined") {
		goto=parseInt(list.style.marginLeft)+custom
	} else {
		goto = parseInt((windowWidth/2)-(thumbnailWidth/2)-(index*thumbnailWidth))

	}
	if (maxLeft > 0) {
		scrollLeft.style.visibility = 'hidden';
   	scrollRight.style.visibility = 'hidden';
		return maxLeft/2
	} else {
		if (goto<=maxLeft) {
			scrollLeft.style.visibility = 'visible';
     	scrollRight.style.visibility = 'hidden';
			return maxLeft;
		} else if (goto>=0) {
			scrollLeft.style.visibility = 'hidden';
     	scrollRight.style.visibility = 'visible';
			return 0
		} else {
			scrollLeft.style.visibility = 'visible';
     	scrollRight.style.visibility = 'visible';
			return goto
		}
	}
}

const ThubnailsAnimate = (from, to) => {
 	let step=8;

 	id = setInterval(frame, 1);
 	function frame() {
		if (from < to) {
    	((to-from)%step>0)?from=from+(to-from)%step:from=from+step;
      list.style.marginLeft = from+'px';
    } else if (from > to ) {
     	((from-to)%step>0)?from=from-(from-to)%step:from=from-step;
     	list.style.marginLeft = from+'px';
    } else {
     	clearInterval(id);
    }
  }
}

const Load = (urlList, index) => {
	current = index
	// if (parseInt(index)<0) {index=urlList.length-1}
	// if ((index+1)<urlList.length) {index=urlList.length-1}
	const img = document.getElementById('image');
	const tmpImg = new Image();
	oldh = parseInt(img.style.height)
	oldw = parseInt(img.style.width)
 	fieldHeight=window.innerHeight-list.offsetHeight-20;
 	fieldWidth=window.innerWidth-20;
  function Resize(oldh, oldw, newh, neww) {
  	wid = setInterval(wframe, 1);
    hid = setInterval(hframe, 1);
    const speed = 10;
    img.style.visibility="hidden";
    function wframe() {
      if (neww > oldw) {oldw++; ((neww-oldw)%speed>0)?oldw=oldw+(neww-oldw)%speed:oldw=oldw+speed; imgframe.style.width = oldw+'px';
      } else if (neww < oldw) {oldw--; ((oldw-neww)%speed>0)?oldw=oldw-(oldw-neww)%speed:oldw=oldw-speed; imgframe.style.width = oldw+'px';
      } else {clearInterval(wid); img.style.width = neww+'px'; if (newh==oldh) { img.style.visibility="visible";} }
    }
    function hframe() {if (newh > oldh) {oldh++; ((newh-oldh)%speed>0)?oldh=oldh+(newh-oldh)%speed:oldh=oldh+speed; imgframe.style.height = oldh+'px';
      } else if (newh < oldh) {oldh--; ((oldh-newh)%speed>0)?oldh=oldh-(oldh-newh)%speed:oldh=oldh-speed; imgframe.style.height = oldh+'px';
      } else {clearInterval(hid); img.style.height = newh+'px'; if (neww==oldw) { img.style.visibility="visible";} }
    }
  }
  tmpImg.src=urlList[index].image;
  tmpImg.onload = function() {
    imgHeight = tmpImg.height;
    imgWidth = tmpImg.width;
    if(window.screen.orientation.type == "portrait-primary"){
    	if((imgHeight*(fieldWidth/imgWidth))>fieldHeight) {imgWidth=imgWidth*(fieldHeight/imgHeight); imgHeight=fieldHeight;}
    	if((imgWidth*(fieldHeight/imgHeight))>fieldWidth) {imgHeight=imgHeight*(fieldWidth/imgWidth); imgWidth=fieldWidth;}
		} else {

	  	if (imgHeight > fieldHeight) {imgWidth = imgWidth*(fieldHeight/imgHeight); imgHeight = fieldHeight; }
   		if (imgWidth > fieldWidth) {imgHeight = imgHeight*(fieldWidth/imgWidth); imgWidth = fieldWidth; }
		}
    Resize(oldh,oldw,imgHeight,imgWidth);
    oldh=imgHeight;
    oldw=imgWidth;
    list = document.getElementById('thumblist');
    // list.style.marginLeft=0+'px'
    img.src=tmpImg.src;
  };
  for (let i=0;i<urlList.length;i++) {
  	if (i==index) {
   		const active = list.getElementsByTagName('img')[i]
   		active.classList.add('active')	
    		// ThubnailsPosition(window.innerWidth/2-(i+1)*(active.offsetWidth+5)+(active.offsetWidth/2))
   		ThubnailsAnimate(parseInt(list.style.marginLeft), ThubnailsPositioning(index))
   	} else {
   		list.getElementsByTagName('img')[i].classList.remove('active')
   	}
  }
  const prevImg = document.getElementsByClassName('previmg')[0]
  const nextImg = document.getElementsByClassName('nextimg')[0]
  if (index>0) {
   	prevImg.href = urlList[index-1].image;
   	prevImg.dataset.index = index-1;
   	prevImg.style.visibility = 'visible';
  } else {
   	prevImg.href = '';
   	prevImg.style.visibility = 'hidden';
  }
  if (index < (urlList.length)-1) {
   	nextImg.href = urlList[parseInt(index)+1].image;
   	nextImg.dataset.index = parseInt(index)+1;
   	nextImg.style.visibility = 'visible';
  } else {
   	nextImg.href = '';
   	nextImg.style.visibility = 'hidden';
  } 
  scrollIndex = index
}

const Start=(urlList, index, mobile) => {
  hidden = document.getElementById('hidden');
  hidden.innerHTML = '<span class="close">&times;</span>'
    +'<div id="maingallery"><div id="photo"><img id="image"></div></div>'
    +'<div id="thumbnails"><div id="thumblist"></div><a class="scrollleft">&#10094</a><a class="scrollright">&#10095</a></div></div>';

  const scrollLeft = document.getElementsByClassName('scrollleft')[0]
  const scrollRight = document.getElementsByClassName('scrollright')[0]

  list = document.getElementById('thumblist');
  imgframe = document.getElementById('photo');
	const prevImg = document.createElement('a');
	const nextImg = document.createElement('a');

	prevImg.innerHTML = "&#10094";
	nextImg.innerHTML = "&#10095";

  hidden.style.display = 'block';
	document.getElementById('maingallery').appendChild(prevImg);
  document.getElementById('maingallery').appendChild(nextImg);
  prevImg.classList.add('previmg');
  nextImg.classList.add('nextimg');
  img = document.getElementById('image');
	img.style.height=100+'px'
	img.style.width=100+'px'
  thumblist = list.getElementsByTagName('img');
  urlList.map((el,i)=>{
  	list.innerHTML += '<img src="'+el['thumbnail']+'" data-index="'+i+'">';
  })

  list.style.marginLeft=ThubnailsPositioning(index)+'px';
  [].forEach.call(thumblist, function(thumb, i) { 
    thumb.addEventListener('click', function() { 
      Load(urlList,this.dataset.index);
    });
  });
  prevImg.addEventListener('click',event=>{
  	event.preventDefault()
  	Load(urlList, event.target.dataset.index)
  })
  nextImg.addEventListener('click',event=>{
  	event.preventDefault()
  	Load(urlList, event.target.dataset.index)
  })
  if(mobile==false) {
  	let scrollIndex = 0;
  	scrollLeft.addEventListener("click", function(){ 
  		scrollIndex-=400;
  		ThubnailsAnimate(parseInt(list.style.marginLeft), ThubnailsPositioning(parseInt(scrollIndex), 400))
  	});
  	scrollRight.addEventListener("click", function(){ 
      console.log(scrollIndex)
  		scrollIndex+=400;
  		ThubnailsAnimate(parseInt(list.style.marginLeft), ThubnailsPositioning(parseInt(scrollIndex), -400))
  	});
  } else {
  	var startPos;
  	let outside;
  	imgframe.addEventListener('touchstart', function(event){
    	startPos = event.touches[0].clientX;
    	pos = parseInt(imgframe.style.marginLeft);
  	});
    imgframe.addEventListener('touchmove', function(event){
      newPos = parseInt((event.touches[0].clientX)-startPos);
      imgframe.style.marginLeft = newPos+'px';
    });
  	let mobileIndex = parseInt(index);
   	imgframe.addEventListener('touchend', function(event){
      if ((newPos < -400) && (parseInt(mobileIndex)<parseInt(urlList.length)-1)) {
      		outside = parseInt(window.innerWidth)/2
      			mobileIndex++
      } else if ((newPos > 400) && parseInt(mobileIndex)>0){
      	mobileIndex--
				outside = -(parseInt(window.innerWidth)/2)
      } else {
      	outside = Math.round(parseInt(imgframe.style.marginLeft)/10)*10
      }
      id = setInterval(frame, 5);
 			function frame() {
 				if(outside>0) {
 					outside-=10
 					imgframe.style.marginLeft=outside+'px'
 				} else if(outside<0) {
 					outside+=10
 					imgframe.style.marginLeft=outside+'px'
 					} else {
 				clearInterval(id)
 				}
 			}
  	Load(urlList, mobileIndex)
  	TouchThumbnails(list);
  	});
 	}
  document.onkeydown = function(event) {
    switch (event.keyCode) {
      case 37:
      	current>0?Load(urlList,parseInt(current)-1):null
        break;
      case 39:
      	current<(urlList.length-1)?Load(urlList,parseInt(current)+1):null
        break;
      case 27:
      	hidden.style.display='none';
      	hidden.innerHTML = "";
        break;
    }
	};
  Load(urlList, index)
}

const Init = (gallery) => {
	let urlList = [];
	const hidden = document.createElement('div');
	document.body.appendChild(hidden);
	hidden.id = 'hidden';
	[].forEach.call(gallery.getElementsByTagName('a'), (image, i) => {
		image.dataset.index = i;
		let singleUrl ={};
		singleUrl['image'] = image.href
		singleUrl['thumbnail'] = image.getElementsByTagName('img')[0].getAttribute('src')
		urlList.push(singleUrl);
		image.addEventListener("click", function(event){
      event.preventDefault();
      Start(urlList, image.dataset.index, isMobile());
      index = image.dataset.index;
     	});
	})
	function isMobile() {
	const match = window.matchMedia || window.msMatchMedia;
		if(match) {
      const mq = match("(pointer:coarse)");
      return mq.matches;
  	}
  return false;
	}
}

document.addEventListener('DOMContentLoaded', function() {
	Init(document.getElementsByClassName('gallery')[0]);
  document.addEventListener('click', function(event) {
    if ( event.target.id=='hidden' || event.target.id=='maingallery' || event.target.className=='close') {
      hidden.style.display='none';
      hidden.innerHTML = "";
    }
  }, false);
}); 